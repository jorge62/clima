## Contenido
Rama principal master


## Clonar el proyecto

- clona el proyecto via ssh => git clone git@gitlab.com:jorge62/clima.git
- o mediante https          => git clone https://gitlab.com/jorge62/clima.git

## Instalacion 
- Entra en la carpeta del repositorio " cd clima "
- Luego a la carpeta del cliente " cd client"
- Ejecuta el comando " npm install "
- vuelta atras ( cd .. ) y entra a la carpeta del server " cd server"
- Ejecuta el comando " npm install "

## Script disponibles para ejecucion en dev

# En el directorio client
### `npm start` 
### `npm run build` - Solo para produccion
# En el directorio server
### `npm run dev` 

# Inicio de sesion
- User:   paola@loop.com
- passwd: Holapaola




# Instalaciones docker y kubernetes

## Requisitos
  
  ### Instalacion docker en Unix
  - 1. Descargue https://download.docker.com/linux/static/stable/x86_64/docker-18.03.1-ce.tgz
  
  - 2. descomprimir =>  tar xzvf docker-18.03.1-ce.tgz
  
  - 3. Copie el archivo binario en el directorio /usr/bin  => cp docker/* /usr/bin/


  ### Instalacion docker en windows
  - 1. Descargue https://download.docker.com/win/static/stable/x86_64
  
  - 2. En powershell 
     para extraer e instalar los archivos                   => Expand-Archive /path/to/<FILE>.zip -DestinationPath $Env:ProgramFiles
     Para registrar el servicio e iniciar el motor docker   => $Env:ProgramFiles\Docker\dockerd --register-service
                                                            => Start-Service docker
     verificar que docker esta instalado y correr una imagen => $Env:ProgramFiles\Docker\docker run hello-world:nanoserver      
  
  ### Instalacion docker-compose en unix
  - 1. Para descargar => sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  
  - 2. Aplicar permisos => sudo chmod +x /usr/local/bin/docker-compose
  
  - 3. crear enlace simbolico => sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
  
  - 4. verificar version => docker-compose --version



  ### Instalcion docker-compose en windows
  - 1. descargar e instalar el .exe => https://hub.docker.com/editions/community/docker-ce-desktop-windows/
  
  ### Instalacion Minikube linux
  - 1. curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
  
  - 2. sudo install minikube-linux-amd64 /usr/local/bin/minikube


  ### Instalacion Minikube windows
  - 1. https://minikube.sigs.k8s.io/docs/start/
  
  - 2. En powershell =>
        oldPath = [Environment]::GetEnvironmentVariable('Path', [EnvironmentVariableTarget]::Machine)
        if ($oldPath.Split(';') -inotcontains 'C:\minikube'){ `
        [Environment]::SetEnvironmentVariable('Path', $('{0};C:\minikube' -f $oldPath), [EnvironmentVariableTarget]::Machine) `
        }   




# DEPLOYS


## Deploy front en minikube
- minikube start
- eval $(minikube docker-env) (en unix)
- minikube docker-env | Invoke-Expression ( en powershell )
- docker build -t testclimaclient:0.0.11 .
- kubectl apply -f frontend-deployment.yaml

## Deploy back en minikube
- minikube start
- eval $(minikube docker-env)  (en unix)
- minikube docker-env | Invoke-Expression ( en powershell )
- docker build -t testclimaserver:0.0.9 .
- kubectl apply -f backend-deployment.yaml

## revisamos los pod
- kubectl get pods
## revisamos los servicios
- kubectl get svc
## revisamos los deploys
- kubectl get deployment
## iniciamos el dashboard
- minikube dashboard
## y obtenemos la URL del deploy 
- minikube service climaclient --url



