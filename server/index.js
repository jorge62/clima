const express = require("express");
const routes = require("./routes");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
require("dotenv").config({ path: "variables.env" });
const cors = require("cors");

//conectar a mongo
mongoose.Promise = global.Promise;
mongoose.connect(process.env.DB_URL, {
  useNewUrlParser: true,
});

//creando servidor
const app = express();

//habilitar recursos para compartir entre server y client
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

//rutas de la app
app.use("/", routes());

//Verificar si la aplicación del servidor está configurada para escuchar solo las solicitudes provenientes de localhost,
const host = '0.0.0.0' || process.env.HOST;
const port = process.env.PORT || 5000;

app.listen(port,host,()=>{
  console.log( "port: " + port, "host: "+ host )
});
