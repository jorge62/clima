const express = require("express");
const router = express.Router();
const usuarioController = require("../controllers/usuarioController");

module.exports = () => {
  //usuario
  router.post("/crear-cuenta", usuarioController.registrarUsuario);
  router.post("/iniciar-sesion", usuarioController.iniciarSesion);

  return router;
};
