const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const Usuario = require("../models/Usuarios");

exports.registrarUsuario = async (req, res) => {
  const usuario = new Usuario(req.body);
  usuario.password = await bcrypt.hash(req.body.password, 12);

  try {
    await usuario.save();
    res.json({ mensaje: "Usuario creado correctamente" });
  } catch (e) {
    console.log(e);
    res.json({ mensaje: "hubo un error" });
  }
};

exports.iniciarSesion = async (req, res, next) => {
  const { email, password } = req.body;
  const usuario = await Usuario.findOne({ email });
  if (!usuario) {
    res.json({ mensaje: "No existe el usuario", status: 400 });
    next();
  } else {
    //si el usuario existe tengo que comprar la pass
    if (!bcrypt.compareSync(password, usuario.password)) {
      res.json({ mensaje: "La contraseña es incorrecta", status: 400 });
      next();
    } else {
      const token = jwt.sign(
        {
          email: usuario.email,
          id: usuario._id,
        },
        "LLAVESECRETA",
        {
          expiresIn: "1h",
        }
      );
      res.json({ token });
    }
    next();
  }
};
