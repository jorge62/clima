import React, { Fragment, useContext } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { ClimaContext, ClimaProvider } from './Clima-Context/Clima-Context';
import Clima from "./components/clima/Clima";
import Header from "./components/layout/Header";
import Footer from "./components/layout/footer";

function App() {
  //use ClimaContext

  const [auth, guardarAuth] = useContext(ClimaContext);

  return (
    <Router>
      <Fragment>
        <ClimaProvider value={[auth,guardarAuth]}>
         <Header />
         <div className="grid contenedor contenido-principal">
            <main className="col-12">
                <Routes>
                  <Route path="/" element={<Clima />} />
                  <Route path="/clima" element={<Clima />} />
                  <Route path="/iniciar-sesion" element={<Clima />} />
               </Routes>
            </main>
         </div>
        </ClimaProvider>
        <Footer />
      </Fragment>
    </Router>
  );
}

export default App;
