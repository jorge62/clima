import React, { useState, useContext} from 'react';

const ClimaContext = React.createContext([{}, () => {}]);

const ClimaProvider = (props) => {
  const token = localStorage.getItem('token') || '';
  const user = localStorage.getItem('user');

  const [auth, guardarAuth] = useState({
    token: token,
    auth: token ? true : false,
    user: user,
  });

  return (
    <ClimaContext.Provider value={[auth, guardarAuth]}>
      {props.children}
    </ClimaContext.Provider>
  );
};

export { ClimaContext, ClimaProvider };
