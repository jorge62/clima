import React from "react";
import {
  MDBFooter,
  MDBContainer,
  MDBCol,
  MDBRow,
  MDBIcon,
} from "mdb-react-ui-kit";

const Footer = () => {
  return (
    <MDBFooter className='bg-dark text-white text-center text-lg-left footer'>
      <div className='text-center p-4 footer-text' style={{ backgroundColor: 'rgba(0, 0, 0, 0.0)' }}>
        &copy; {new Date().getFullYear()} Copyright:{' '}
        All right register |
      </div>
    </MDBFooter>
  );
};

export default Footer;
