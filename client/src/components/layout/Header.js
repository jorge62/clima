import React, { Fragment, useContext, useEffect } from "react";
import { useNavigate } from "react-router";
import Swal from "sweetalert2";
import Login from "../auth/Login";
import { ClimaContext} from '../../Clima-Context/Clima-Context';

const Header = () => {
  const [auth, guardarAuth] = useContext(ClimaContext);
  useEffect(()=>{

  })

  const navigate = useNavigate();

  const cerrarSesion = () => {
    Swal.fire({
      title: "¿Esta segur@?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, cerrar sesion!",
    }).then((result) => {
      if (result.isConfirmed) {
        //clean token
        guardarAuth({
           token: "",
           auth: false,
           user:"",
         });
        localStorage.setItem("token", "");
        localStorage.setItem("user", "");
        localStorage.clear();
        navigate("/");
      }
    });
  };

  return (
    <Fragment>
      <header className="barra">
        <div className="contenedor">
          <div className="contenido-barra">
            <h1></h1>
            {auth.auth ? (
              <button
                type="button"
                className="custom-btn btn-3"
                aria-haspopup="true"
                aria-expanded="false"
                onClick={cerrarSesion}
              >
                CERRAR SESIÓN
                <i className="fas fa-user"></i>
              </button>
            ) : (
              <Login />
            )}
          </div>
        </div>
      </header>
    </Fragment>
  );
};

export default Header;
