import { Fragment, useContext, useState } from "react";
import { useNavigate } from "react-router";
import Swal from "sweetalert2";
import clienteAxios from "../../config_axios/axios";
import { ClimaContext} from '../../Clima-Context/Clima-Context';

const Login = () => {
  const [login, guardarLogin] = useState({});
  const navigate = useNavigate();
  const [auth, guardarAuth] = useContext(ClimaContext);
  

  const leerDatos = (e) => {
    guardarLogin({
      ...login,
      [e.target.name]: e.target.value,
    });
    
  };

  const submitForm = async (e) => {
    e.preventDefault();
    
    await clienteAxios
      .post("/iniciar-sesion", login)
      .then((response) => {
        if (response.data.status === 400) {
          Swal.fire({
            title: "Error",
            text: response.data.mensaje,
          });
        } else {
        
          //extrayendo token y almacenando en localStorage
          const { token } = response.data;
          localStorage.setItem("token", token);
          localStorage.setItem("user", login.email);
           guardarAuth({
             token,
             auth: true,
             user: login.email,
           });

          Swal.fire("Sesión iniciada", "", "success");
          navigate("/clima");
        }
      })
      .catch((e) => {
        Swal.fire({
          title: "Error",
          text: e,
        });
      });
  };

  return (
    <Fragment>
      <div className="btn-group">
        <button
          type="button"
          className="custom-btn btn-3"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          INICIAR SESIÓN
          <i className="fas fa-user"></i>
        </button>
        <div className="dropdown-menu dropdown-menu-center">
          <form onSubmit={submitForm} className="px-4 py-3">
            <div className="form-group">
              <span className="usuarioLogin">Usuario</span>
              <input
                type="text"
                name="email"
                placeholder="Email"
                required
                onChange={leerDatos}
              />
            </div>
            <br></br>
            <div className="form-group">
              <span className="passwordLogin">Contraseña</span>
              <br></br>
              <input
                type="password"
                name="password"
                placeholder="Password"
                required
                onChange={leerDatos}
              />
            </div>
            <br></br>
            <br></br>
            <br></br>
            <button type="submit" className="custom-btn btn-13">
              Iniciar Sesion
            </button>
          </form>
          <div className="dropdown-divider"></div>
        </div>
      </div>
    </Fragment>
  );
};

export default Login;
