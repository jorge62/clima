import React, { useEffect, useState} from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBRow,
  MDBCol,
} from "mdb-react-ui-kit";
import Spinner from "react-bootstrap/Spinner";
import Reporte from "./Reporte";



const ClimaDatos = () => {
  const [zona, guardarZona] = useState({});
  
  const [loading, setLoading] = useState(false);
  const [loadingReporte, setLoadingReporte] = useState(false);

  useEffect(()=>{
    setLoadingReporte(false)
  },[zona]);

  const leerDatos = (e) => {
    guardarZona({
      ...zona,
      [e.target.name]: e.target.value,
    });
  };

  const getWeather = async (e) => {
    e.preventDefault();
    
    setLoading(true);
    
    setTimeout(() => {
      setLoading(false);
      setLoadingReporte(true)
    }, 2000);
    
  };


  return (
    <MDBRow>
      <MDBCol sm="6" className="card1">
        <MDBCard alignment="center" className="cardClimaDatos">
          <h1 className="cardClimaDatosSubtitle font-weight-bold">
            Seleccioná la zona
          </h1>
          <MDBCardBody>
            <form onSubmit={getWeather}>
              <span className="pais">País</span>
              <input
                type="text"
                id="fname"
                name="pais"
                placeholder="Selecciona un país"
                onChange={leerDatos}
                required
              />
              <br></br>
              <span className="ciudad">Ciudad</span>
              <input
                type="text"
                id="lname"
                name="ciudad"
                placeholder="Seleccioná una ciudad"
                onChange={leerDatos}
                required
              />
              <br></br>
              <br></br>
              {!loading ? (
                <input type="submit" value="Buscar" />
              ) : (
                <Spinner animation="grow" />
              )}
            </form>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>
      <MDBCol sm="6" className="card2">
          <MDBCard alignment="center" className="cardClimaDatos">
            <h1 className="cardClimaDatosSubtitle font-weight-bold">Reporte</h1>
            <MDBCardBody>
             {loadingReporte && !loading ? (<Reporte zona = {zona} loading={loading} loadingReporte = {loadingReporte}/>) : ""}
             {loading ? <Spinner animation="grow" /> : ""}   
            </MDBCardBody> 
          </MDBCard> 
      </MDBCol> 
      
    </MDBRow>
  );
};

export default ClimaDatos;
