import React from "react";


const ClimaHoy = (props) => {

    const {data} = props;
    const {timezone,city_name,weather,temp,country_code,rh,wind_spd,precip} = data;

    //obtengo temp en fahrenheit
    let fahrenheit = (temp * 1.8) + 32
    fahrenheit = Number.parseFloat(fahrenheit).toFixed(1)

    //obtengo pais
    const regionNames = new Intl.DisplayNames(
      ['es'], {type: 'region'}
    ); 
    const pais = regionNames.of(country_code);

    //obtengo dia
    const dias = [ 
      'domingo',
      'lunes',
      'martes',
      'miércoles',
      'jueves',
      'viernes',
      'sábado',
    ];
    let fecha = new Date().getDay();
    dias.map((e,index)=>{if(index === fecha) fecha = e})
    
    return (
    <div>
      <div className="today-section-zone">
        <div className="zone-info">
          <p>{pais}</p>
          <p className="city">{city_name}</p>
          <img src={`https://www.weatherbit.io/static/img/icons/${data.weather.icon}.png`} alt="" className="img"/>
          <div className="description">
            <p className="fecha">{fecha.toUpperCase()}</p>
            <p className="weather-des">{weather.description.toUpperCase()}</p>
          </div>
        </div>
      </div>

      
      <div>
        <div className="temp">
          <span className="temp-celcius">{temp}°</span>
          <span className="temp-fahrenheit">{fahrenheit}°F</span>
        </div>
        <div className="ave-tempt">
          <p >Prob. de precipitaciones: {precip === null ? '' : Number.parseFloat(precip).toFixed(0)}%</p>
          <p className="rh">Humedad: {rh === null ? '' : Number.parseFloat(rh).toFixed(0)}%</p>
          <p className="wind">Viento: a {wind_spd === null ? '' : Number.parseFloat(wind_spd).toFixed(0)} km/h.</p>
        </div>
      </div>
    </div>
    
  );
};

export default ClimaHoy;