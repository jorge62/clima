import React, { useEffect} from "react";
import ClimaHoy from './ClimaHoy';
import useFetch from "../../hooks/useFetch";


const Reporte = (props) => {
    const {zona} = props;

    useEffect(()=>{
    },[])


    let { ciudad, pais } = zona;
    pais = pais.toLowerCase();
    ciudad = ciudad.toLowerCase();
    const WEATHER_KEY = "44ad1974047742c39fd0d7df5853decc";
    const API_URL = `https://api.weatherbit.io/v2.0/current?lang=es&city=${ciudad}&country=${pais}&key=${WEATHER_KEY}&include=minutely`;
    let {data, notData} = useFetch(API_URL,zona);
   

    return(
        <div>
            {notData ? " " : ( 
                <ClimaHoy data={data}/>
            )}
        </div>)
}

export default Reporte;