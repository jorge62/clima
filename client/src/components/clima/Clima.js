import React, { useEffect, useContext } from "react";
import ClimaDatos from "./ClimaDatos";
import { ClimaContext} from '../../Clima-Context/Clima-Context';

const Clima = () => {
  const [auth, guardarAuth] = useContext(ClimaContext);

  useEffect(() => { 
  },[]);
  return auth.user === null || auth.user === 'undefined' || auth.user === '' ? (
    <div>
      <p className="font-weight-bold climatitle">SERVICIO DEL CLIMA</p>
    </div>
  ) : (
    <div>
      <p className="font-weight-bold climatitle">SERVICIO DEL CLIMA</p>
      <ClimaDatos />
    </div>
  );
};

export default Clima;
