import { useState, useEffect } from "react";
import Swal from "sweetalert2";
import { useNavigate } from 'react-router';

const useFetch = (url,zona) => {
  const [loadingReportefetch, setLoadingReporte] = useState(false);
  const [notData,setNotData] = useState(true);
  const [data, guardarData] = useState({});

  const navigate = useNavigate();

  useEffect(() => {
    async function fetchData() {
      let { ciudad, pais } = zona;
      const response = await fetch(url);
      
        if(response.ok){
          if(response.statusText === "OK"){
             const fetchData = await response.json();
            //validar pais-ciudad
            //obtengo pais para validar
            const regionNames = new Intl.DisplayNames(
              ['es'], {type: 'region'}
            ); 
          

            let paisFetch = regionNames.of(fetchData.data[0].country_code);
            let ciudadFetch =  fetchData.data[0].city_name
            //remuevo caracteres especiales
            const removeAccents = (str) => {
              return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
            } 

            pais = removeAccents(pais);
            paisFetch =  removeAccents(paisFetch);
            ciudad = removeAccents(ciudad);
            ciudadFetch = removeAccents(ciudadFetch);

            if(JSON.stringify(ciudadFetch).trim().toLocaleLowerCase() !== JSON.stringify(ciudad).trim().toLocaleLowerCase() || JSON.stringify(paisFetch).trim().toLocaleLowerCase() !== JSON.stringify(pais).trim().toLocaleLowerCase()){
               Swal.fire({
                  title: "Error",
                  text: "No hay datos disponibles",
               });
               setNotData(true);
               navigate('/');
            }else{
              guardarData(fetchData.data[0])
              setLoadingReporte(true)
              setNotData(false)
             
            }   
          
          }else{
              Swal.fire({
              title: "Error",
              text: "No hay datos disponibles",
            });
            setNotData(true)
             navigate('/');
          }
        }else{
         Swal.fire({
               title: "Error",
               text: response.data,
         });
         setLoadingReporte(false);
         navigate('/');
        }   


    }
    fetchData();
  }, [loadingReportefetch]);
  return { data, loadingReportefetch, notData};
};

export default useFetch;